<?php
/**
 * @file
 * Taxonomy display handler
 */

/**
 * Add a display handler that will use the given function to render the display.
 */
class TaxonomyDisplayAssociatedDisplayHandlerFunction extends TaxonomyDisplayAssociatedDisplayHandler {

  /**
   * Build our output to be rendered to the user.
   *
   * @see TaxonomyDisplayAssociatedDisplayHandler::displayAssociated()
   */
  public function displayAssociated($term, $options = NULL) {
    $markup = call_user_func($options['function'], $term, $options);
    $build['function'] = array(
      '#markup' => $markup,
    );
    return $build;
  }

  /**
   * Build our form for the fieldset.
   *
   * @see TaxonomyDisplayHandlerForm::formFieldset()
   */
  public function formFieldset(&$form, &$values, $options = NULL) {

    $form['#description'] = t('Use a <em>Function</em> for displaying associated content.');
    //fetch a list of registered functions
    $functions = module_invoke_all('taxonomy_display_callback_info');
    $radio_options = array_combine($functions, $functions);

    $form['function'] = array(
      '#description' => t('Choose the function you would like to use to display the associated content.'),
      '#title' => t('Function'),
      '#type' => 'radios',
      '#options' => $radio_options,
      '#default_value' => isset($options['function']) ? $options['function'] : FALSE,
    );

  }

  /**
   * We store values to access later for rendering and editing.
   *
   * @see TaxonomyDisplayHandlerForm::formSubmit()
   */
  public function formSubmit($form, &$values) {
    // We are using the exact keys that our formFieldset() implementation
    // defines and we want all of the values stored, so we have no need to alter
    // them before returning.
    return $values;
  }
}
